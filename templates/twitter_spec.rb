require 'spec_helper'

describe package('monit') do
  it { should be_installed }
end

describe service('monit') do
  it { should be_running }
end

describe package('firefox') do
  it { should be_installed.with_version('15.0.1-0.el5') }
end

describe package('dejavu-lgc-fonts') do
  it { should be_installed }
end

describe package('x11vnc') do
  it { should be_installed }
end

describe package('perl-IPC-Run') do
  it { should be_installed }
end


