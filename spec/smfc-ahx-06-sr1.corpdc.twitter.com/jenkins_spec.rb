require 'spec_helper'

describe package('jenkins') do
  it { should be_installed.with_version('1.518-1.1') }
end

describe service('jenkins') do
  it { should be_running }
  it { should be_monitored_by('monit') }
end

describe package('java-1.6.0-openjdk-1.6.0.0') do
  it { should be_installed }
end

describe yumrepo('jenkins') do
  it { should exist }
end

describe yumrepo('twitter-firefox') do
  it { should exist }
end

describe file('/usr/bin/gitwrapper') do
  it { should be_file }
  it { should be_mode 755 }
  it { should be_owned_by 'root' }
end

describe file('/data') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins/plugins') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins/bin') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins/bin/manage_mysql.sh') do
  it { should be_file }
  it { should be_mode 755 }
  it { should be_owned_by 'root' }
end

describe file('/data/jenkins/bin/my.cnf') do
  it { should be_file }
  it { should be_mode 644 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins/bin/cigit') do
  it { should be_file }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/data/jenkins/bin/cigit.cfg') do
  it { should be_file }
  it { should be_mode 644 }
  it { should be_owned_by 'jenkins' }
end

describe file('/var/lib/jenkins/.m2/settings.xml') do
  it { should be_file }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/var/lib/jenkins/.ivy2') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/var/lib/jenkins/.ivy2/artifactory-credentials.properties') do
  it { should be_file }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/var/log/jenkins') do
  it { should be_directory }
  it { should be_mode 755 }
  it { should be_owned_by 'jenkins' }
end

describe file('/etc/my.cnf') do
  it { should_not be_file }
end

describe file('/etc/sysconfig/jenkins') do
  it { should be_file }
  it { should contain "JENKINS_HOME=\"/data/jenkins\"" }
end
