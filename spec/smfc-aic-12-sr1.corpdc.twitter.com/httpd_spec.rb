require 'spec_helper'

describe package('httpd') do
  it { should be_installed }
end

describe service('httpd') do
  it { should be_enabled   }
  it { should be_running   }
end

describe port(80) do
  it { should be_listening }
end

describe file('/etc/httpd/sites-available/jvm-ci3.twitter.biz.conf') do
  it { should be_file }
  it { should contain "ServerName jvm-ci3.twitter.biz" }
end
