#!/bin/bash

SERVERS=$( loony -d group -g role:jenkins.master list | awk '{print $1}' )

for server in $SERVERS; do
  if [ ! -d "$server" ]; then
    mkdir -p spec/$server
  fi

  cp -R templates/* spec/$server/

done  
